# Terraform project integration introduced in GitLab 13.5
- [Infrastructure as code with Terraform and GitLab](https://docs.gitlab.com/ee/user/infrastructure/)
    - if not using GitLab's HTTP Backend for Terraform state, then specify [TF_HTTP_* variables](https://www.terraform.io/docs/backends/types/http.html#configuration-variables)
- This gitlab-ci.yml includes [Terraform.latest.gitlab-ci.yml](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Terraform.latest.gitlab-ci.yml) which includes [Terraform/Base.latest.gitlab-ci.yml](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Terraform/Base.latest.gitlab-ci.yml)
- creates a DynamoDB in AWS ca-central-1. AWS region defined in variables.tf - [see DynamoDB tables in AWS console](https://ca-central-1.console.aws.amazon.com/dynamodb/home?region=ca-central-1#tables:group=$ALL)
